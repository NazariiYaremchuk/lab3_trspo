package com.yaremchuk.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.yaremchuk.security.entities.User;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
}