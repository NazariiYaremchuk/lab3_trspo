package com.yaremchuk.security.services;

import com.yaremchuk.security.entities.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}